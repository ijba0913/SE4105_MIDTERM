import React from 'react'
import Col from './Column'
import { Grid } from 'react-bootstrap'

const statusList = ['pending', 'preparing', 'delivered']
let newUpdate = false
let sumLimit = 1
let delay = 0

class Screen extends React.Component {
  constructor(props) {
    super(props)
    this.state = { orders: [], orderLength: 0 }
    this.getOrders = this.getOrders.bind(this)
    this.checkUpdate = this.checkUpdate.bind(this)
    this.solveFibonacci = this.solveFibonacci.bind(this)
  }

  async getOrders() {
    const orders = []
    let orderLength = 0

    for (const status of statusList) {
      const query = await fetch('/getOrdersByStatus/' + status)
      const list = await query.json()
      orderLength += list.length
      orders.push({ status, list })
    }

    this.setState({ orders, orderLength })

    const tick = async () => {
      await this.checkUpdate()
      if (newUpdate || delay >= 300000) {
        delay = 0
        sumLimit = 1
      }
      else {
        delay = this.solveFibonacci(0, 1)
      }

      const now = new Date()
      console.log('~~~ delay ', delay, ' sumLimit ', sumLimit, ' time ', now.getSeconds())
      console.log('~~~~~~~~~~', delay, '~~~~~~~~~~~')
      console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
      setTimeout(tick, delay);
    }
    setTimeout(tick, delay);
  }

  async checkUpdate() {
    const query = await fetch('/getAllOrders')
    const list = await query.json()

    if (list.length != this.state.orderLength) {
      newUpdate = true
      await this.getOrders()
    }
    else {
      newUpdate = false
    }
  }

  solveFibonacci(n1, n2) {
    let sum = n1 + n2
    if (sum <= sumLimit) {
      return this.solveFibonacci(n2, sum)
    }

    sumLimit += n1
    return n1 * 1000
  }

  componentDidMount() {
    this.getOrders()
  }

  render() {
    const orders = this.state.orders.map((order) => {
      return (
        <Col
          key={this.state.orders.indexOf(order)}
          status={order.status}
          list={order.list}
          onStatusChange={this.getOrders}
          statusList={statusList} />
      )
    })

    return <Grid>{orders}</Grid>
  }
}

export default Screen