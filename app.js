const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const webpack = require('webpack')
const config = require('./webpack.config')
const devMiddleware = require('webpack-dev-middleware')
const hotMiddleware = require('webpack-hot-middleware')
const { MongoClient, ObjectId } = require('mongodb')
const url = "mongodb://localhost:27017/se4105"
let db;

async function startServer() {
  const app = express()
  db = await MongoClient.connect(url)

  if (process.env.NODE_ENV !== 'production') {

    const compiler = webpack(config)

    app
      .use(devMiddleware(compiler, {
        noInfo: true, publicPath: config.output.publicPath
      }))
      .use(hotMiddleware(compiler))
  }

  app
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use('/', express.static(path.join(process.cwd(), 'public')))
    .listen(3000, () => { console.log('server live') })

  app.get('/getAllOrders', getAllOrders)
  app.get('/getOrdersByStatus/:status', getOrdersByStatus)
  app.post('/updateStatus/:id&:status', updateStatus)
}

async function getAllOrders(request, response) {
  const status = request.params.status
  const orders = await db.collection("orders").find().toArray()
  response.json(orders)
}

async function getOrdersByStatus(request, response) {
  const status = request.params.status
  const orders = await db.collection("orders").find({ status }).toArray()
  response.json(orders)
}

async function updateStatus(request, response) {
  const id = new ObjectId(request.params.id)
  const status = request.params.status
  console.log(id, status)
  const result = await db.collection("orders").updateOne({ _id : id }, { $set: { status: status }})
  response.json(result)
}

startServer()