import React from 'react'
import Order from './Order'
import { Col } from 'react-bootstrap'

class Column extends React.Component {
  render() {
    let title = this.props.status.toLowerCase().replace(/\b(\w)/g, s => s.toUpperCase());


    const contents = this.props.list.map((order) => {
      return (
        <Order
          key={order._id}
          id={order._id}
          number={this.props.list.indexOf(order) + 1}
          contents={order.contents}
          status={this.props.status}
          onStatusChange={this.props.onStatusChange}
          statusList={this.props.statusList} />
      )
    })

    return (
      <Col xs={6} md={4}  >
        {this.props.list.length} {title}  Orders
        {contents}
      </Col>
    )
  }
}


export default Column