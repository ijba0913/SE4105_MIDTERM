import React from 'react'
import ReactDom from 'react-dom'
import Screen from './Screen'

ReactDom.render(<Screen />, document.getElementById("root"))