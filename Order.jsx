import React from 'react'
import { Panel, Button, ButtonGroup } from 'react-bootstrap'

class Order extends React.Component {
  constructor(props) {
    super(props)
    this.handleStatusChange = this.handleStatusChange.bind(this)
    this.getButtons = this.getButtons.bind(this)
  }

  async handleStatusChange(number) {
    const { id, status, statusList, onStatusChange } = this.props
    const newStatus = statusList[statusList.indexOf(status) + number]
    const url = `/updateStatus/${id}&${newStatus == null ? status : newStatus}`
    await fetch(url, { method: 'POST' })
    onStatusChange()
  }

  getButtons() {
    const sListLength = this.props.statusList.length
    const status = this.props.status
    const firstColumn = this.props.statusList[0]
    const lastColumn = this.props.statusList[sListLength - 1]

    if (status == firstColumn) {
      return <Button onClick={() => this.handleStatusChange(1)} bsStyle="success"> next </Button>
    } else if (status == lastColumn) {
      return <Button onClick={() => this.handleStatusChange(-1)} > previous </Button>
    }

    return (
      <ButtonGroup bsSize="small">
        <Button onClick={() => this.handleStatusChange(-1)} > previous </Button>
        <Button onClick={() => this.handleStatusChange(1)} bsStyle="success"> next </Button>
      </ButtonGroup>
    )
  }

  render() {
    const contents = this.props.contents.map((content, index) => {
      return <li key={index}>{content}</li>
    })
    
    return (
      <div>
        <Panel>
          <Panel.Body>{contents}</Panel.Body>
          <Panel.Footer>
            {this.getButtons()}
          </Panel.Footer>
        </Panel>
      </div>
    )
  }
}

export default Order